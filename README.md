This extension will stop stores from going down because of the known "Front controller reached 100 router match iterations" issue.

It Overrode the Mage_Core_Model_Config and forced $_useCache = false when regenerating the config.

This fix has been for a long in the shades, because we decided that it's not the final solution for the "100 router" issue. We haven't created a repository for it up until few days ago, so you will find it installed manually on many stores.

This is a temporary and tested fix for the issue "Front controller reached 100 router match iterations", based on this Magento certified developer deep investigation: https://github.com/convenient/magento-ce-ee-config-corruption-bug#the-fix

After a while, Magento published a patch to solve this issue for Magento EE only; PATCH_SUPEE-4755_EE_1.13.1.0_v1.sh